from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt

from .models import Contenido


# Create your views here.

def index(request):
    """
    Si me hacen un GET /cms/ le doy una respuesta de que esta es una app de CMS
    """
    contenidos = Contenido.objects.all()
    if request.user.is_authenticated:
        logged = ("Logged in as " + request.user.username +
                  " <a href='/cms/logout'>Logout</a>")
    else:
        logged = "Not logged in. <a href='/admin/'>Login via admin</a>"
    return render(request, "userrr/index.html",
                  {"logged": logged, "contenidos":
                      contenidos, "request": request})


@csrf_exempt
def do_MEHTOD(request, llave):
    """
    En caso de que el método sea GET, obtengo
    el body del objeto guardado c
    on la llave = llave
    y devuelvo su contenido.
    En caso de no existir contenido
    para esa llave salta la excepción.
    En caso de que el método sea PUT,
    obtengo el cuerpo de la petición
    y me creo un objeto del tipo contenido,
    guardando su llave y su valor.
    Luego devuelvo el body del objeto creado.
    """

    html = ""
    if request.method == "GET":
        try:
            contenido = Contenido.objects.get(llave=llave)
            return render(request, "userrr/body.html",
                          {"llave": contenido.llave, "valor": contenido.valor})
        except Contenido.DoesNotExist:
            logout(request)
            return redirect("/cms/")

    elif request.method == "PUT":
        cuerpo = request.body.decode("utf-8")
        if cuerpo:
            contenido = Contenido(llave=llave, valor=cuerpo)
            contenido.save()
        try:
            respuesta = Contenido.objects.get(llave=llave).valor
            html = (f"Guardando en un nuevo contenido "
                    f"la llave {llave} con el cuerpo : {respuesta}")
        except Contenido.DoesNotExist:
            html += f"No hay contenido para la llave: {llave}"
    elif request.method == "POST":
        new_content = request.POST["cuerpo"]
        try:
            c = Contenido.objects.get(llave=llave)
            c.valor = new_content
            c.save()
            return render(request, "userrr/body.html",
                          {"llave": llave, "valor": new_content})
        except Contenido.DoesNotExist:
            html += f"No hay contenido para la llave: {llave}"

    return HttpResponse(html)


@csrf_exempt
def logout_view(request):
    logout(request)
    return redirect("/cms/")
